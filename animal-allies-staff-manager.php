<?php
/*
Plugin Name: Animal Allies Staff Manager
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Staff Manager Plugin for Custom Fields
Version:     2017.17.02
Author:      WordPress.org
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

/**
 * HTML for the plugin options page.
 * There are currently no options really, so
 * this exists to perpare if options are necessary.
 */
function wporg_options_page_html()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "wporg_options"
            settings_fields('wporg_options');
            // output setting sections and their fields
            // (sections are registered for "wporg", each field is registered to a specific section)
            do_settings_sections('wporg');
            // output save settings button
            submit_button('Save Settings');
            ?>
        </form>
    </div>
    <?php
}

/**
 * Sets up the options page and its location in the menu
 */
function wporg_options_page()
{

    add_options_page(
        'WPOrg Options',
        'WPOrg Options',
        'manage_options',
        'wporg',
        'wporg_options_page_html'
    );

}

/**
 * Custom post type creation for staff members and wish list.
 */
function create_post_type() {
    // staff members custom post
    register_post_type( 'staff_members',  // slug
        array(
            'labels' => array(
                'name'          => __( 'Staff' ),
                'singular_name' => __( 'Staff' )
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies'  => array( 'category' ), // let posts have categories
        )
    );

    // wish list custom post
    register_post_type( 'wish_list',  // slug
        array(
            'labels' => array(
                'name'          => __( 'Wish List' ),
                'singular_name' => __( 'Wish List' ),
            ),
            'public'      => true,
            'has_archive' => true,
            'taxonomies'  => array( 'category' ), // let posts have categories
        )
    );
}
add_action( 'init', 'create_post_type' );

